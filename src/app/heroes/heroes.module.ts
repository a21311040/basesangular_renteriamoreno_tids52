import { NgModule } from '@angular/core';
import { HeroeComponent } from './heroe/heroe.component';
import { ListadoComponent } from './listado/listado.component';
import { CommonModule } from '@angular/common';


@NgModule({
    declarations: [ //Las declaraciones dicen cuales componentes contiene este modulo
        HeroeComponent,
        ListadoComponent
    ],
    exports: [  //Los exports indican cuales componentes seran publicos.
        ListadoComponent // es decir, fuera de este modulo.
    ],
    imports: [
        CommonModule // en los imports van los modulos.
    ]
})
export class HeroesModule {

}