import { Component } from '@angular/core';


@Component({
    selector: 'app-contador',
    template: `
    <h1>{{title}}</h1>
    <h2>La base es <strong>{{base}}</strong></h2>

    <button (click)="acumular(1);">+1</button>
    <span>{{numero}}</span>
    <button (click)="acumular(-1);">-1</button>`
})
export class ContadorComponent {
    title = 'Contador App';
    numero: number = 10;
    base: number =5;
  
    acumular (valor:number){
      this.numero += valor;
    }
}